function generate_tb() {
	let row_number = document.getElementById('row_number');
	let col_number = document.querySelector('#col_number');
	let sym_number = document.getElementsByTagName('input')[2];
	let all = document.getElementsByTagName('input');
	let tb = "<table>"
		for(let i=0; i<row_number.value; i++) {
			tb += "<tr>"
				for(let j=0; j<col_number.value; j++){
					tb += "<td>"
					tb += generate_r_diff_string(sym_number.value)
					tb += "</td>"
				}
			tb += "</tr>"
		}
	tb += "</table>"
	let div_tb = document.getElementById('div-tb');
	div_tb.innerHTML = tb;
}

function generate_r_diff_string(N) {
	let abc = "abcdefghijklmnopqrstuvwxyz"
	let w = ""
	while(w.length!=N) {
		let s = abc[Math.floor(Math.random()*abc.length)]
		if(w.indexOf(s)==-1) {
			w += s
		}
	}
	return w
}

generate_r_diff_string(7)