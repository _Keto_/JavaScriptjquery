document.addEventListener("DOMContentLoaded", function () {
  const daysOfWeekInput = document.getElementById("daysOfWeek");
  const buttons = document.querySelectorAll("#myButton");

  const days = ["ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი", "კვირა"];

  buttons.forEach((button, index) => {
    button.addEventListener("click", function () {
      daysOfWeekInput.value = days[index];
    });
  });
});
