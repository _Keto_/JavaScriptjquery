const person = {
    name: "Keti",
    'lastname': "Makhatadze",
    "education": {
        "higher": "Yes",
        University: "Georgian-American University",
        GPA: 4.3
    },
    hobbies:["Coding", "Travelling", "Watch Movies"],
    getFullName:function(){
        console.log(this.name, this.lastname)
    }
}

console.log(person)
console.log(person.name)
console.log(person['name'])
console.log(person.education.GPA)
console.log(person.hobbies[1])
person.getFullName()
