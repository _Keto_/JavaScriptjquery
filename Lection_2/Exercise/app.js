// -----TASK 1-----
function task_1(num) {
    console.log(Math.ceil(num))
}

task_1(5,6)



// -----TASK 2-----
function task_2(num){
    console.log(Math.floor(num))
}

task_2(9,3)


// -----TASK 3-----
function task_3(num){
    console.log(Math.round(num))
}

task_3(5.9)


// ამის ქვემოთ იმიტომ დავაკომენტარე რომ ყველა ფუნქციას ერთად ვერ ვამუშავებდი, ყველაზე ერთი და იგივე პასუხი გამოჰქონდა

// -----TASK 5-----
// function getRandomNumber() {
//     const random = Math.random(); 
//     const randomNumber = random * (0, 1);
//     return randomNumber;
// }

// const randomNum = getRandomNumber();

// console.log(randomNum);


// -----TASK 6-----
// function getRandomNumber() {
//     const random = Math.random(); 
//     const randomNumber = random * (5, 50);
//     return randomNumber;
// }

// const randomNumb = getRandomNumber();

// console.log(randomNumb);
  

// -----TASK 11-----
function getRandomDayOfWeek() {
    const daysOfWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    const randomIndex = Math.floor(Math.random() * daysOfWeek.length);
    return daysOfWeek[randomIndex];
  }
  
const randomDay = getRandomDayOfWeek();
  
console.log(randomDay);