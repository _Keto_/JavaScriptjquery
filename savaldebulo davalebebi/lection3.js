// -----13-----
// function inputString(n) {
//     let length = n.length;
//     return (length * (length + 1)) / 2;
// }

// let n = "My name is Keti, I love volleyball";
// let x = inputString(n);
// console.log(x);

// -----1-----
// function stringLength(s) {
//     return s.length;
// }

// let myString = "This is Homework, Exercise Number one";
// let length = stringLength(myString);
// console.log(`The length of the string is: ${length}`);

// -----2-----
function myString(a){
    let count = 0;
    for(let i = 0; i < a.length; i++){
        if (a[i] === 'a'){
            count++;
        }
    }
    return count;
}

let string = myString("My string is awesome and JavaScript is good")
console.log(string)
  