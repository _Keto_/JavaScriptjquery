// -----1-----
// document.addEventListener('DOMContentLoaded', function () {
//     const container = document.querySelector('.container');
//     const box = document.querySelector('.box');
  
//     const buttons = document.querySelectorAll('.boxes button');
  
//     function updateBoxPosition() {
//       box.style.top = boxPosition.top + 'px';
//       box.style.left = boxPosition.left + 'px';
//     }
  
//     function isWithinBoundaries() {
//       const containerRect = container.getBoundingClientRect();
//       const boxRect = box.getBoundingClientRect();
  
//       return (
//         boxRect.top >= containerRect.top &&
//         boxRect.bottom <= containerRect.bottom &&
//         boxRect.left >= containerRect.left &&
//         boxRect.right <= containerRect.right
//       );
//     }
  
//     let boxPosition = { top: 0, left: 0 };
  
//     buttons.forEach(function (button, index) {
//       button.addEventListener('click', function () {
//         switch (index) {
//           case 0:
//             boxPosition.top += 10;
//             break;
//           case 1:
//             boxPosition.top -= 10;
//             break;
//           case 2:
//             boxPosition.left -= 10;
//             break;
//           case 3:
//             boxPosition.left += 10;
//             break;
//           case 4:
//             boxPosition.top += 10;
//             boxPosition.left -= 10;
//             break;
//           case 5:
//             boxPosition.top += 10;
//             boxPosition.left += 10;
//             break;
//           case 6:
//             boxPosition.top -= 10;
//             boxPosition.left -= 10;
//             break;
//           case 7:
//             boxPosition.top -= 10;
//             boxPosition.left += 10;
//             break;
//         }
  
//         if (isWithinBoundaries()) {
//           updateBoxPosition();
//         } else {
//           boxPosition = { top: parseFloat(box.style.top), left: parseFloat(box.style.left) };
//         }
//       });
//     });
//   });
  

// -----2-----
function appendContent() {
    const obj = {
    };
  
    const htmlToAppend = "<p>This is some appended content.</p>";
  
    appendHTML(obj, htmlToAppend);
  }
  
  function appendHTML(obj, html) {
    const container = document.getElementById("container");

    container.innerHTML = `<h2>${obj.title}</h2>`;
  
    container.innerHTML += html;
  }