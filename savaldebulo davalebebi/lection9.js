function zoomIn(element) {
    element.style.transform = 'scale(1.5)';
}

function zoomOut(element) {
    element.style.transform = 'scale(1)';
}