// -----1-----
// alert("გამარჯობა მე ვარ დიალოგური ფანჯარა alert");
// function showAlert() {
//     document.body.innerHTML += "<h1>კეთილი იყოს თქვენი მობრძანება საიტზე!!!!!</h1>";
// }

// showAlert()

// -----2-----
// var userConfirmed = window.confirm("გამარჯობაა, გსურთ გაიგოთ ვებ გვერდზე უფრო მეტი??????");

// if (userConfirmed) {
//     document.body.innerHTML += "<h1 style='color: white;'><span style='color: red;'> გილოცავთ,</span> თქვენ სწორი არჩევანი გააკეთეთ</h1>";
//     document.body.innerHTML += "<h4 style='color: yellow;'>ამ ვებ გვერდზე მრავალი საინტერესო მაგალითი შეგხვდებათ ვებ გვერდის აწყობის შესახებ</h4>";
//     document.body.innerHTML += "<h4 style='color: white;'>ვებ გვერდის აწყობის ტექნოლოგიები: </h4>";
//     document.body.innerHTML += "<li style='color: white;'>HTML - საფუძვლები</li>";
//     document.body.innerHTML += "<li style='color: white;'>CSS - სტილები</li>";
//     document.body.innerHTML += "<li style='color: white;'>JavaScript - გაფორმება </li>";
//     document.body.innerHTML += "<li style='color: white;'>PHP - პროგრამირება</li>";


// } else {
//     document.body.innerHTML += "<h1 style='color: green;'>დაინტერესდით ვებ გვერდის აწყობის ტექნოლოგიით.</h1>";
//     document.body.innerHTML += "<h4 style='color: white;'>დაეუფლეთ მსოფიოში ერთ-ერთ ყველაზე მოთხოვნად პროფესიას</h4>";
//     document.body.innerHTML += "<h4 style='color: yellow;'>ისწავლე და დასაქმდი! </h4>";
// }

// -----4-----
function myTime() {
    let currentDate = new Date();
  
    let year = currentDate.getFullYear();
    let month = currentDate.getMonth() + 1 ; 
    let dayOfMonth = currentDate.getDate();
    let dayOfWeek = currentDate.getDay();
    let hour = currentDate.getHours();
    let minute = currentDate.getMinutes();
    let second = currentDate.getSeconds();
    let millisecond = currentDate.getMilliseconds();
  
    console.log('Year:', year);
    console.log('Month:', month);
    console.log('Day:', dayOfMonth);
    console.log('Day of the Week:', dayOfWeek);
    console.log('Hour:', hour);
    console.log('Minute:', minute);
    console.log('Second:', second);
    console.log('Millisecond:', millisecond);
}
  
myTime();