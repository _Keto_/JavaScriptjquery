// -----2-----
// const box = document.getElementById('box');
// const cursorSquare = document.getElementById('cursor-square');

// box.addEventListener('mousemove', (event) => {
//     const mouseX = event.clientX - box.getBoundingClientRect().left;
//     const mouseY = event.clientY - box.getBoundingClientRect().top;

//     cursorSquare.style.left = mouseX - cursorSquare.clientWidth / 2 + 'px';
//     cursorSquare.style.top = mouseY - cursorSquare.clientHeight / 2 + 'px';

//     cursorSquare.style.display = 'block';
// });

// box.addEventListener('mouseleave', () => {
//     cursorSquare.style.display = 'none';
// });

// -----3-----
// const container = document.getElementById('container');
// const colors = ['green', 'blue', 'red', 'yellow'];

// for (let y = 0; y < 500; y += 50) {
//     for (let x = 0; x < 600; x += 50) {
//     const color = colors[Math.floor(Math.random() * colors.length)];

//     const square = document.createElement('div');
//     square.className = `color-square ${color}`;
//     square.style.left = `${x}px`;
//     square.style.top = `${y}px`;

//     container.appendChild(square);
//     }
// }

// -----6-----
document.addEventListener('DOMContentLoaded', function () {
    const scale = document.querySelector('.scale');
    const mark = document.getElementById('mark');
    const rangeInput = document.getElementById('rangeInput');
  
    rangeInput.addEventListener('input', function () {
      const value = parseInt(rangeInput.value);
  
      const newPosition = (value / 100) * (scale.offsetWidth - mark.offsetWidth);
  
      mark.style.left = newPosition + 'px';
    });
  });
  