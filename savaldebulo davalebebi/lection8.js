// -----5-----
// const form = document.getElementById("form");
// const table = document.getElementById("table");
// const multiplesOfFive = document.getElementById("multiplesOfFive");

// form.addEventListener("submit", (event) => {
//   event.preventDefault();

//   const m = parseInt(document.getElementById("m").value);
//   const n = parseInt(document.getElementById("n").value);

//   let multiplesOfFiveCount = 0;

//   table.innerHTML = "";

//   const headerRow = table.insertRow();
//   for (let i = 1; i <= n; i++) {
//     const headerCell = headerRow.insertCell();
//     headerCell.textContent = i;
//   }

//   for (let i = 1; i <= m; i++) {
//     const row = table.insertRow();
//     row.insertCell().textContent = i;

//     for (let j = 1; j <= n; j++) {
//       const cell = row.insertCell();
//       const multiple = i * 3 * j;

//       if (multiple % 3 === 0) {
//         cell.textContent = multiple;
//         cell.classList.add(multiple % 2 === 0 ? "even" : "odd");

//         if (multiple % 5 === 0) {
//           multiplesOfFiveCount++;
//         }
//       }
//     }
//   }
// });

// -----1-----
function generateNumbers() {
    const n = parseInt(document.getElementById('count').value);
    const k = parseInt(document.getElementById('lowerBound').value);
    const p = parseInt(document.getElementById('upperBound').value);
  
    if (isNaN(n) || isNaN(k) || isNaN(p) || n <= 0 || k > p) {
      alert("Please enter valid values.");
      return;
    }
  
    const oddNumbers = [];
    for (let i = 0; i < n; i++) {
      let randomNumber = Math.floor(Math.random() * (p - k + 1)) + k;
  
      if (randomNumber % 2 === 0) {
        randomNumber++;
      }
  
      oddNumbers.push(randomNumber);
    }
  
    const resultDiv = document.getElementById('result');
    resultDiv.innerHTML = `Generated odd numbers: ${oddNumbers.join(', ')}`;
  }
  