function f_example_1() {
    document.write("Hello Keti! <br>")
}

f_example_1()

function f_example_2(name) {
    document.write("Hello " + name + "!<br>")
}

f_example_2("Lizi")
f_example_2("Tako")

function f_example_3(lastname) {
    document.write("Hello " + lastname + "!<br>")
}

f_example_3("Lobjanidze")
f_example_3("Tutisani")

function f_example_4(name, lastname) {
    document.write("Hello " + name +" "+ lastname + "!<br>")
}

f_example_4("Lizi", "Lobjanidze")
f_example_4("Tako", "Tutisani")

// function f_example_5(N) {
//     if(N > 10) {
//         document.write("Graten than 10")
//     }else{
//         document.write("less than 10")
//     }
// }

// f_example_5(16)

function f_example_6(N) {
    if(N < 10) {
        document.write("Less than 10")
    }else if(N < 100){
        document.write("Number is between 100")
    }else{
        document.write("Graten tha 100")
    }
}

f_example_6(45)

k = 77

function f_example_7() {
    for(i=9; i<20; i+=3) {
        document.write("<p>JS FOR LOOP " +i+" </p>");
    }

    while(k<100) {
        document.write(k+"<br>")
        k++
    }
}

f_example_7()