var mydivs = document.querySelectorAll("div")
// console.log(mydivs)
mydivs[0].classList.add("container")
mydivs[1].classList.add("menu")

function add_circle(){
    let circ =  document.createElement("div")
    // console.log(circ)
    circ.classList.add("circle")
    circ.style.backgroundColor = random_color()
    const d = 2*random_radius()
    circ.style.width = d+"px"
    circ.style.height = d+"px"
    // mydivs[0].appendChild(circ)
    // mydivs[0].insertBefore(circ)
    // if(mydivs[0].children.length==0){
    //     mydivs[0].appendChild(circ)
    // }else{
    //     mydivs[0].insertBefore(circ, mydivs[0].children[0])
    // }
    circ.style.top = Math.floor(Math.random()*(300-d))+"px";
    circ.style.left = Math.floor(Math.random()*(800-d))+"px";
    mydivs[0].insertBefore(circ, mydivs[0].firstChild)
    // console.log(mydivs[0].children)
}

function random_color(){
    const r = Math.floor(Math.random()*256)
    const g = Math.floor(Math.random()*256)
    const b = Math.floor(Math.random()*256)
    return "rgb("+r+","+g+","+b+")"
}

function random_radius(){
    return Math.floor(Math.random()*46)+5;
}